const constants = {
    TEST_ENVIRONMENT_PORT: 1711,
    PRODUCTION_PORT: 1710,
}

export default constants;