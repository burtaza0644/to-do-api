// Database conection
import mongoose from 'mongoose';

const prodConnection = 'mongodb+srv://service:AyeNKOv5ltw0GCl0@tododb.kckcr.mongodb.net/TodoDb?retryWrites=true&w=majority';
const testConnection = 'mongodb+srv://service:AyeNKOv5ltw0GCl0@tododb.kckcr.mongodb.net/TestDb?retryWrites=true&w=majority';

const DB = {
    connect: async () => {
        try {
            await mongoose.connect(prodConnection, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
            console.log('DB CONNECTION OK');
            return true;
        }
        catch (ex) {
            console.log('CONNECTION ERROR', ex.toString());
            return false;
        }
    },
    mock: {
        connect: async () => {
            try {
                await mongoose.connect(testConnection, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
                await mongoose.connection.dropDatabase();
                console.log('DB CONNECTION OK');
                return true;
            } catch (ex) {
                console.log('CONNECTION ERROR', ex.toString());
                return false;
            }
        },
        // To drop database and close the connection and stop mongo server
        close: async () => {
            await mongoose.connection.dropDatabase();
            await mongoose.connection.close();
        },
        // To remove all data for all db collections.
        refresh: async () => {
            await mongoose.connection.dropDatabase();
        }
    },
}

export default DB;