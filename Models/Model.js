import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const model = mongoose.model;
import toJson from '@meanie/mongoose-to-json';

const Todo = new Schema({
    toDo: {
        type: String,
        required: true,
        unique: false
    },
}, { timestamps: true });

Todo.plugin(toJson);

export const ToDo = model('Todo', Todo, 'Todo');