import { Router } from "express";
import UserService from "../Services/UserService.js";

const UserController = Router();

export const SetTodo = async (request, response, next) => {
    const { description } = request.body;
    let result = await UserService.createNewTodoItem(description);
    if (!result.error) {
        response.json(await UserService.getTodoList());
        return;
    }
    response.json(result);
    return;
}

UserController.post('/SetTodo', SetTodo);

export default UserController;