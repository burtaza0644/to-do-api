import { ToDo } from "../Models/Model.js";
// services
export default {
    getTodoList: async () => {
        try {
            let list = await ToDo.find({}, { _id: 0, createdAt: 0, updatedAt: 0, __v: 0 });
            let result = list.map((item, index) => {
                const { toDo } = item;
                return {
                    id: ++index,
                    toDo
                }
            });
            return { data: result, error: false, message: '' }
        } catch (ex) {
            return { data: null, error: true, message: '' }
        }
    },
    createNewTodoItem: async toDo => {
        try {
            if (toDo) {
                await ToDo.create({ toDo });
                return { data: null, error: false, message: '' }
            }
            else {
                return { data: null, error: true, message: 'An error occured' }
            }
        } catch (ex) {
            return { data: null, error: true, message: 'An error occured' }
        }
    }
}