
# to-do-api

  

<table>

<tr>

<td>

A Node JS REST API for saving user's todo list to remote [MongoDB](https://www.mongodb.com/) database in Todolist app.

</td>

</tr>

</table>

  
  

## Demo

Here is a working live demo : http://34.125.216.15:1017/api

## Usage

### Endpoints

There is no authentication in this api, all endpoints are public.

#### /User/SetTodo

##### Expected Headers

"Content-Type" : "application/json"

##### Expected Request Body
You should send data by valid JSON format. Following line is example of reqest body:
`{ "description" : "WILL_TODO" }`
  

## SETUP

  

- [ ]  Clone this repo to your desktop and run `npm install` to install all the dependencies.
- [ ]  Run `npm start`  for start server.
  

## Development

This app was developed by adopting TDD.

### File system

#### Controllers

Contains .js scripts for routing endpoints to functions.

#### DataContext

Contains scripts about database connections.

##### connection.js

Exports the module for connecting real or mock database with functions. Uses [Mongoose](https://www.npmjs.com/package/mongoose).
#### Helpers

Contains global constants (formatters, enums, helpful functions etc.).

#### Models

Models about DB Documents for Code First configuration. Uses Mongoose as mongo client.

#### Services

Interfaces about Database operations.

#### Tests

Contains test scripts for application like unit test, pact test, integration test


  

## Testing and Deployment
  

## Test and Deploy

-  #### Uses the built-in continuous integration and continuous delivery process in GitLab.
### CI/CD
- #### When pushed code to main branch, will trigger CI/CD pipeline.  The process runs at Gitlab Runner which runs on a remote virtual machine and contains running all test and deploying application. Pipeline configuration code is  located on  .gitlab-ci.yml file.
 #### Pipeline
##### The following steps are applied sequentially:
-	Build App
-	Run unit tests, integration tests, contract tests on local network
-	Deploy app to test environment
-	Run contract tests again on test environment
-	Deploy app to production
	
 
### Deployment Tools
#### Deployment is by using Docker
	- Connects to the remote virtual machine with Docker installed via ssh in an automated CD process.
	- Pulls this repo to machine and create Docker image from Dockerfile.
	- Runs the container from related image.

### Testing Tools
- [Mocha](https://mochajs.org/)
- [Supertest](https://www.npmjs.com/package/supertest)
- [Pact](https://pact.io/)
