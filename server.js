import { startServer } from "./startServer.js";

const server = startServer(8080);

export default server;