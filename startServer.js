//startup

import express from 'express';
import cors from 'cors';
import UserController from './Controllers/UserController.js';
import DB from './DataContext/connection.js';
import constants from './Helpers/Constants.js';

export const service = express();
service.use(express.urlencoded({ extended: true }));
service.use(express.json());
service.use(cors());
service.use('/api/User', UserController);


export const startServer = async (deployPort, mockServer = false) => {

  if (mockServer || deployPort === constants.TEST_ENVIRONMENT_PORT)
    await DB.mock.connect();
  else
    await DB.connect();

  return service.listen(deployPort, (err) => {
    if (err) {
      console.log('err thrown', err.stack);
      return err;
    }
    console.log('Server online on ' + deployPort);
    return;
  });
};

