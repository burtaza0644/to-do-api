import UserService from '../../Services/UserService.js';
import DB from '../../DataContext/connection.js';
import assert from 'assert';

describe("User Service Unit Test", async () => {
    before(async () => {
        await DB.mock.connect();
        return;
    });
    after(async ()=> {
        await DB.mock.close();
    });
    it('getUserList should get all toDo item from db', async () => {
        const data = await UserService.getTodoList();
        assert.ok(data);
        assert.ok(Array.isArray(data.data));
    });
    it('createNewTodoItem should return object which error property is false', async () => {
       const data = await UserService.createNewTodoItem('New Todo');
       assert.ok(data != null)
       assert.ok(!data.error);
    });
    it('when data is null or empty, createNewTodoItem should return object which error property is true', async () => {
        let data = await UserService.createNewTodoItem(null);
        assert.ok(data != null)
        assert.ok(data.error);
        data = await UserService.createNewTodoItem('');
        assert.ok(data != null)
        assert.ok(data.error);
     });
});