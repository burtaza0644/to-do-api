import path from 'path';
import { Verifier } from '@pact-foundation/pact';
import { startServer } from '../../startServer.js';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import assert from 'assert';
import DB from '../../DataContext/connection.js';
import constants from '../../Helpers/Constants.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Pre-verify provider contract in local network
describe('Pact Provider verification', () => {
    let server;
    before(async () => {
        server = await startServer(constants.TEST_ENVIRONMENT_PORT, true);
    });

    it('Should validate the consumer', async () => {
        try {
            const verifier = new Verifier({
                providerBaseUrl: `http://localhost:${constants.TEST_ENVIRONMENT_PORT}`,
                pactUrls: [path.resolve(
                    __dirname,
                    './todoapp-todoapi.json'
                )]
            });
            await verifier.verifyProvider();
            server.close();
            await DB.mock.refresh();
            process.exit(0);
        } catch (ex) {
            console.log('ERROR: ', ex)
            server.close();
            await DB.mock.refresh();
            process.exit(1);
        }
    })
});