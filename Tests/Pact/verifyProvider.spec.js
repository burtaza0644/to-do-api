import path from 'path';
import { Verifier } from '@pact-foundation/pact';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import assert from 'assert';
import DB from '../../DataContext/connection.js';
import constants from '../../Helpers/Constants.js';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// verify provider on test deployment
describe('Pact Provider verification', () => {
  let server;
  before(async () => {
    await DB.connect();
    await DB.mock.refresh();
  });

  it('Should validate the consumer', async () => {
    try {
      const verifier = await new Verifier({
        providerBaseUrl: `http://34.125.216.15:${constants.TEST_ENVIRONMENT_PORT}`,
        pactUrls: [path.resolve(
          __dirname,
          './todoapp-todoapi.json'
        )]
      });
      await verifier.verifyProvider();
      await DB.mock.refresh();
      process.exit(0);
    } catch (ex) {
      console.log('ERROR: ', ex)
      await DB.mock.refresh();
      process.exit(1);
    }
  })
});