import request from 'supertest';
import assert from 'assert';
import DB from '../../DataContext/connection.js';
import { service } from '../../startServer.js';

describe("Set todo item", () => {
    before(async () => {
        return await DB.mock.connect();
    });
    after(async () => {
        return await DB.mock.close();
    });
    it('set todo should insert db and return to do list', async () => {
        const response = await request(service).post('/api/User/SetTodo').send({ description: 'test todo' });
        const { data, error } = response.body;
        assert.ok(Array.isArray(data));
        assert.ok(!error);
        assert.ok(response.status === 200);
    });

    it('set todo should send error in body', async () => {
        const response = await request(service).post('/api/User/SetTodo').send({ description: null });
        const { data, error, message } = response.body;
        assert.ok(data === null);
        assert.ok(message === 'An error occured');
        assert.ok(error);
        assert.ok(response.status === 200);
    });
});